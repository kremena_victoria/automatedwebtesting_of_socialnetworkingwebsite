package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SecureAreaPage {
    private WebDriver driver;
    private By successfulLoginAlert = By.tagName("span");

    public SecureAreaPage(WebDriver driver){
        this.driver = driver;
    }

    public String getLoginAlert(){
        return driver.findElement(successfulLoginAlert).getText();
    }
}
