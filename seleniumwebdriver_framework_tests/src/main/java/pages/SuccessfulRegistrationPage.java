package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SuccessfulRegistrationPage {

    private WebDriver driver;
    private By registrationStatusAlert = By.cssSelector(".container span");

    public SuccessfulRegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    public String getAlertText(){
        return driver.findElement(registrationStatusAlert).getText();
    }
}
