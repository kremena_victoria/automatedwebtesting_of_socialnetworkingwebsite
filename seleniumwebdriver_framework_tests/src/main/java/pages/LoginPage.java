package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private WebDriver driver;

    private By loginEmailField = By.id("username");
    private By loginPasswordField = By.id("password");
    private By loginButton = By.id("login-submit");

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public void setUserEmail(String email){
        driver.findElement(loginEmailField).sendKeys(email);
    }

    public void setUserPassword(String pass){
        driver.findElement(loginPasswordField).sendKeys(pass);
    }

    public SecureAreaPage clickLoginButton(){
        driver.findElement(loginButton).click();
        return new SecureAreaPage(driver);
    }
}
