package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;
    private By registrationLink = By.linkText("Registration");
    private By loginPageLink = By.linkText("Log in");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public RegistrationPage clickRegistrationLink(){
        driver.findElement(registrationLink).click();
        return new RegistrationPage(driver);
    }

    public LoginPage clickLoginLink(){
        driver.findElement(loginPageLink).click();
        return new LoginPage(driver);
    }
}
