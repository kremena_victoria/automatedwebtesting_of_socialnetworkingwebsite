package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {
    private WebDriver driver;

    private By firstNameField = By.id("firstName");
    private By lastNameField = By.id("lastName");
    private By emailField = By.id("email");
    private By confirmEmailField = By.id("confirmEmail");
    private By passwordField = By.id("password");
    private By confirmPasswordField = By.id("confirmPassword");
    private By agreeTermsCheckbox = By.id("terms");
    private By registerButton = By.cssSelector(".form-group button");

    public RegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    public void setUserFirstName(String userfirstname){
        driver.findElement(firstNameField).sendKeys(userfirstname);
    }

    public void setUserLastName(String userlastname){
        driver.findElement(lastNameField).sendKeys(userlastname);
    }

    public void setUserEmail(String email){
        driver.findElement(emailField).sendKeys(email);
    }

    public void confirmUserEmail(String email){
        driver.findElement(confirmEmailField).sendKeys(email);
    }

    public void setPassword(String pass){
        driver.findElement(passwordField).sendKeys(pass);
    }

    public void confirmPassword(String pass){
        driver.findElement(confirmPasswordField).sendKeys(pass);
    }

    public void checkCheckBox(){
        //driver.findElement(agreeTermsCheckbox).getAttribute("checked").equals("true");

        if ( !driver.findElement(agreeTermsCheckbox).isSelected() )
        {
            driver.findElement(agreeTermsCheckbox).click();
        }
    }

    public SuccessfulRegistrationPage clickRegistrationButton (){
        driver.findElement(registerButton).click();
        return new SuccessfulRegistrationPage(driver);
    }
}
