package registration;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.RegistrationPage;
import pages.SuccessfulRegistrationPage;

public class RegistrationTests extends BaseTests {

    @Test
    public void testSuccessfulRegistration(){
        RegistrationPage registrationPage = homePage.clickRegistrationLink();
        registrationPage.setUserFirstName("Kremena");
        registrationPage.setUserLastName("Stoyanova");
        registrationPage.setUserEmail("kremeja@yahoo.com");
        registrationPage.confirmUserEmail("kremenja@yahoo.com");
        registrationPage.setPassword("qwerty345");
        registrationPage.confirmPassword("qwerty345");
        registrationPage.checkCheckBox();
        SuccessfulRegistrationPage successfulRegistrationPage = registrationPage.clickRegistrationButton();
        Assert.assertEquals(successfulRegistrationPage.getAlertText(),
                "Toggle navigation",
                "Alert text is incorrect!");
    }
}
