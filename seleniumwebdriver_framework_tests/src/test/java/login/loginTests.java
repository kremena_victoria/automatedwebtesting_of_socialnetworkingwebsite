package login;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SecureAreaPage;

public class loginTests extends BaseTests {

    @Test
    public void testSuccessfulLogin(){
        LoginPage loginPage = homePage.clickLoginLink();
        loginPage.setUserEmail("kremena_victoria@yahoo.com");
        loginPage.setUserPassword("qwerty123");
        SecureAreaPage secureAreaPage = loginPage.clickLoginButton();
        Assert.assertEquals(secureAreaPage.getLoginAlert(), ("kremena_victoria@yahoo.com"),
                "Alert test is incorrect");
    }
}
